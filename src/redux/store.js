import { createStore, applyMiddleware, compose } from "redux";
import thunk from 'redux-thunk';
import reducer from '../reducers/summingReducers';
import { getLocalStorage } from './LocalStorageLoader';


const composeEnhancers = process.env.NODE_ENV !== 'production' && typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

const middleware = applyMiddleware(
    thunk
);

let store;
const preloadedDate = getLocalStorage();
if( preloadedDate === undefined){
    store = createStore( reducer, composeEnhancers( middleware ) );
} else {
    
    store = createStore( reducer, preloadedDate, composeEnhancers( middleware ) ); 
}


export default store;
