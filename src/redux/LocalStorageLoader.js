export const getLocalStorage = () => {
    try {
        const serializedState = localStorage.getItem('places');
        if (serializedState === null) {
            return undefined;
    }
        return JSON.parse(serializedState);
    } catch (err) {
        return undefined;
    }
};

export const setLocalStorage = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('places', serializedState);
    } catch (err) {
     console.log("There was an error trying to save data");
    }
};
