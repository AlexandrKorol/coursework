export const DECLINE_ORDER = 'DECLINE_ORDER';

export const ClearSits = () =>(dispatch)=>{
    dispatch({
        type: DECLINE_ORDER,
    });
}