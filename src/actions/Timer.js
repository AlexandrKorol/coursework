

export const REDUCE_TIME = 'REDUCE_TIME';
export const REFRESH_TIME = 'REFRESH_TIME';
var interval;
export const TimerAction = (busy, time) => (dispatch) => {

    if (!busy) {
        dispatch({
            type: 'DO_BUSY'
        });
        interval = setInterval(() => {
            if (time !== 0) {
                dispatch({
                    type: REDUCE_TIME,
                })
            }
        }, 1000);
    }
    else {
        dispatch({
            type: REFRESH_TIME,
        });
    }
}
export const ClearTime = (time) => (dispatch) => {
    dispatch({
        type: 'MAKE_FREE'
    });
    clearInterval(interval);
}
