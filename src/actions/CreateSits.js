export const CREATE_SITS = 'CREATE_SITS';

export const SitsCreation = () => (dispatch) => {
    let result=[];
    for (let i = 0; i < 60; i++) {
        if (i < 20) {
            result.push({
                id: i,
                type: 'cheap',
                price: 80,
                booked: false
            });
        }
        if (i >= 20 && i < 40) {
            result.push({
                id: i,
                type: 'usual',
                price: 120,
                booked: false
            });
        }
        if (i >= 40 && i < 50) {
            result.push({
                id: i,
                type: 'vip',
                price: 170,
                booked: false
            });
        }
    }
    dispatch({
        type: CREATE_SITS,
        payload: result
    })
}