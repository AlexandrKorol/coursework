export const BOOK_A_TICKET = 'BOOK_A_TICKET';

export const BookATicket = (result, price, active, currentId) => (dispatch) => {

    dispatch({
        type: BOOK_A_TICKET,
        payload: result,
        payload2: price,
        payload3: active,
        payload4: currentId
    })
}