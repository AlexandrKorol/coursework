import Hall from './Hall';
import FacePage from './FacePage';

export default [
    {
        path:'/CinemaHall',
        component: Hall
    },
    {
        path:'/',
        component: FacePage
    }
]