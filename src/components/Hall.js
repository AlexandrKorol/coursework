import React from 'react';
import CinemaHall from './CinemaHall';
import OrderPanel from './OrderPanel';

const Hall = () => {
    return (
        <div className="App">
            <CinemaHall />
            <OrderPanel />
        </div>
    )
}

export default Hall;