import React from 'react';
import { connect } from 'react-redux';
import { ApproveTheOrder } from '../actions/ApproveTheOrder';
import { DeclineSit } from '../actions/DeclineSit';
import {ClearTime} from '../actions/Timer';
import {TimerAction} from '../actions/Timer';
import { setLocalStorage } from '../redux/LocalStorageLoader';
import Timer from './Timer';
const OrderPanel =(props)=> {
    const { activated, price, clickHandler, declineChose, sits, time, busy } = props;
   return(
        
        
        
            <div className='orders'>
                <div><h3>The total price of your tickets is:</h3>{price}UAH </div>
                {
                    activated.length !== 0  &&
                    <Timer />
                    
                }
                
                <button className="approveBtn" onClick={clickHandler(sits,time)}>Approve order</button>
                <div>
                    <ul className="chosen-tab">
                        {
                            activated.length !== 0 && time !== 0 ?
                                activated.map(item => (
                                    <li key={item.id}>A sit #{item.id} was chosen. Price: {item.price}UAH <div className='cross2' onClick={declineChose(item.id, activated, sits, busy, time)}></div></li>
                                )) :
                                null
                        }
                    </ul>
                </div>
            </div>

        )
    
}

const mapStateToProps = (state) => ({
    price: state.sits_reducer.totalPrice,
    sits: state.sits_reducer.sits,
    activated: state.sits_reducer.active,
    time: state.timer_reducer.time,
    busy: state.timer_reducer.busy
})
const mapDispatchToProps = (dispatch) => ({

    clickHandler: (sits, time) => (e) => {
        dispatch(ApproveTheOrder());
        dispatch(ClearTime(time));
        setLocalStorage({
            sits_reducer:{
                sits,
            loaded: false,
            totalPrice: 0,
            active: [],
            }
        });
    },
    declineChose: (id, activated, sits, busy, time) => (e) => {
        let price = 0;

        let resSits = sits.map(item => {
            if (item.id === id) {
                return {
                    ...item,
                    booked: false
                }
            }
            else {
                return item;
            }
        })
        let result = activated.filter(item => {
            if (item.id === id) {
                price = item.price;
            }
            else {
                return item;
            }

        });
        dispatch(DeclineSit(resSits, price, result));
        dispatch(TimerAction(busy, time))
    },
    
});
export default connect(mapStateToProps, mapDispatchToProps)(OrderPanel);
