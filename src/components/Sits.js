import React from 'react';
import { connect } from 'react-redux';
import { BookATicket } from '../actions/BookingATicket';
import {TimerAction} from '../actions/Timer';
const Sits = (props) => {
    const { sits, clickHandler, time,busy } = props;
    return (
        <div className="lounge">
            <div className="screen">
               
                <div className="center_screen">Screen</div>
                
            </div>
            <ul>
                {
                    sits.map(item => {
                        if (item.type === 'cheap') {

                            return item.booked ?
                                <li className='cheap booked' key={item.id}><div className='cross'></div></li> :
                                <li className='cheap' key={item.id} onClick={clickHandler(item.id, sits, busy, time)}></li>
                        }

                    })
                }

            </ul>
            <ul>
                {
                    sits.map(item => {
                        if (item.type === 'usual') {
                            return item.booked ?
                                <li className='usual booked' key={item.id}><div className='cross'></div></li> :
                                <li className='usual' key={item.id} onClick={clickHandler(item.id, sits, busy, time)}></li>

                        }
                    })
                }
            </ul>
            <ul>
                {
                    sits.map(item => {
                        if (item.type === 'vip') {
                            return item.booked ?
                                <li className='vip booked' key={item.id}><div className='cross'></div></li> :
                                <li className='vip' key={item.id} onClick={clickHandler(item.id, sits, busy, time)}></li>
                        }
                    })
                }
            </ul>

        </div>

    )
}
const mapStateToProps = (state) => ({
    sits: state.sits_reducer.sits,
    time: state.timer_reducer.time,
    busy: state.timer_reducer.busy
})

const mapDispatchToProps = (dispatch) => ({
    clickHandler: (id, sits, busy, time) => (e) => {
        let price = 0;
        let active;
        let result = sits.map(item => {
            if (item.id === id) {
                price = item.price;
                active = item;
                return {
                    ...item,
                    booked: !item.booked
                }
            }
            else {
                return item;
            }
        })
        dispatch(BookATicket(result, price, active, id));
        dispatch(TimerAction(busy, time));

    }
});
export default connect(mapStateToProps, mapDispatchToProps)(Sits);