import React from 'react';
import {NavLink} from 'react-router-dom';
const Navbar = () =>{
    return(
        <div className="navbar">
            <ul>
                <NavLink to="/">Home</NavLink>
                <NavLink to="/CinemaHall">Main scene</NavLink>
            </ul>
        </div>
    )
}

export default Navbar;