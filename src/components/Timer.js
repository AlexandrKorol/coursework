import React from 'react';
import { connect } from 'react-redux';
import {ClearTime} from '../actions/Timer';
import {ClearSits} from '../actions/DeclineOrder';
const Timer = (props) => {
    const { time, busy, active, allsits} = props;
    if(time === 0){
        let idis = active.map(item=>{
            return {
                id: item.id
            }
        })
        let workingplace = allsits;

        workingplace.map(big=>{
            idis.map(little=>{
                if(little.id === big.id){
                    big.booked = false
                }
            })
        })
        props.kickAll(active, allsits);
    }
    return time !== 0 && busy &&
    <div className="timer"><h3>Time left to make an order:{time} </h3></div>

}

const mapStateToProps = (state) => ({
    time: state.timer_reducer.time,
    busy: state.timer_reducer.busy,
    active: state.sits_reducer.active,
    allsits: state.sits_reducer.sits
})

const mapDispatchToProps = {
    kickAll: (active, allsits)=>(dispatch)=>{
        dispatch(ClearTime());
        dispatch(ClearSits(active, allsits))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Timer);