import React from 'react';
import '../App.css';
import { Switch, Route } from 'react-router-dom';
import rootRoutes from './rootRoutes';
import Navbar from './Navbar';
function App() {
  return (
    <div >
      <Navbar />
        <Switch>
          {
            rootRoutes.map((route, key) => (
              <Route key={key}
                {...route}
              />
            ))
          }
        </Switch>
    </div>
  );
}

export default App;