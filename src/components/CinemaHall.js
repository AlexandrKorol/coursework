import React from 'react';
import { connect } from 'react-redux';
import { SitsCreation } from '../actions/CreateSits';
import Sits from './Sits';


class CinemaHall extends React.Component {

    state = {
        sits: [],
        loaded: false
    }

    componentDidMount() {
        if (this.props.list.length === 0) {
            this.props.clickHandler();

        }

        this.setState({
            sits: this.props.list,
            loaded: true
        });
        
    }
    render() {
        const { loaded } = this.state;
        return loaded ?
            <div className="sits">
                <Sits />
            </div>:
            <h1>Loading...</h1>
        
       
       
    }
}


const mapStateToProps = (state) => ({
    list: state.sits_reducer.sits,
    allstore: state
})

const mapDispatchToProps = (dispatch) => ({
    clickHandler: () => {
        dispatch(SitsCreation());
    }
});


export default connect(mapStateToProps, mapDispatchToProps)(CinemaHall);