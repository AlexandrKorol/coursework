import {REDUCE_TIME, REFRESH_TIME} from '../actions/Timer';
const inititalState={
    time:60,
    busy: false
}

const timeReducer =(state=inititalState, action)=>{
    switch(action.type){
        case REDUCE_TIME:
            return{
                ...state,
                time: state.time -1,
                
            }
        case REFRESH_TIME:
            return{
                ...state,
                time: 60
            }
        case 'DO_BUSY':
            return{
                ...state,
                busy:true
            }
        case 'MAKE_FREE':
            return{
                time: 60,
                busy: false
            }
        default: 
            return state
            
    }
}
export default timeReducer;