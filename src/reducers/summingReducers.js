import { combineReducers } from 'redux';

import sits_reducer from './sits_reducer';
import timer_reducer from './timer_reducer';

const reducer = combineReducers({
    sits_reducer,
    timer_reducer
});

export default reducer;
