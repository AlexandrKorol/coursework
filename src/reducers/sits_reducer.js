import { BOOK_A_TICKET } from '../actions/BookingATicket';
import { CREATE_SITS } from '../actions/CreateSits';
import { APPROVE_ORDER } from '../actions/ApproveTheOrder';
import { DECLINE_SIT } from '../actions/DeclineSit';
import {DECLINE_ORDER} from '../actions/DeclineOrder';
const initialData = {
    sits: [],
    totalPrice: 0,
    loaded: false,
    active: [], 
    currentId: null
}

const sitsReducer = (state = initialData, action) => {
    switch (action.type) {

        case CREATE_SITS:
            return {
                ...state,
                sits: action.payload
            }
        case BOOK_A_TICKET:
            return {
                ...state,
                sits: action.payload,
                totalPrice: state.totalPrice + action.payload2,
                active: [...state.active, action.payload3],
                currentId: action.payload4

            }
        case DECLINE_SIT:
            return {
                ...state,
                sits: action.payload,
                totalPrice: state.totalPrice - action.payload2,
                active: action.payload3
            }
        case DECLINE_ORDER:
            return{
                ...state,
                totalPrice:0,
                active:[],
                
            }
        case APPROVE_ORDER:
            return {
                ...state,
                totalPrice: 0,
                active: []
            }
       
        default:
            return state
    }
}
export default sitsReducer;